<?php get_header(); ?>

<div id="register" class="form__content">
    <?php get_template_part('template-parts/content', 'billing-info'); ?>
</div>

<section class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="banner-heading">Trainings for the <br> Clients and Partners</h1>
                <img class="sales-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/smlogo.png" alt="SALESmanago logo">
            </div>
        </div>
    </div>
</section>

<nav class="client-partner-nav">
    <ul>
        <li><a class="agenda-link partner-link">Partner</a></li>
        <li><a class="agenda-link klient-link">Client</a></li>
    </ul>
</nav>

<div id="description">
    <section class="description description-klient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="training-description">
                        <h3>We would like to invite you on a one-day training for all SALESmanago clients. </h3>
                        <h4><strong>The event is free.</strong></h4>
                        <h4>Time and place: <strong>COMING SOON</strong></h4>
                        <br>
                        <p>The purpose of the training is providing knowledge about the SALESmanago Marketing Automation system. If you are just to begin to create your own Marketing Automation compain,you will definitely need to know how to do it.</p>
                        <p>Of course you have all the sources for learning: </p>
                        <ul>
                            <li>webinars</li>
                            <li>online course on the SALESmanago e-learning platform </li>
                            <li>support page</li>
                            <li>ebooks etc</li>
                        </ul>
                        <p>But during the training, we will show you how to go through the first 10 steps of the system configuration: together we will put the first settings on your account - so <strong>do not forget to take your computer with you!</strong></p>
                        <p><strong>Advantages</strong>:</p>
                        <ul>
                            <li>no boring presentations - work in the system !</li>
                            <li>thoughts exchange - brainstorm on the topic of Marketing Automation </li>
                            <li>case study - you do not have to read thousands of ebooks : we will tell you everything </li>
                            <li>after the training you will have an opportunity to set up your system and start working with the SALESmanago Marketing Automation</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ma-triangle"></div>
        </div>
    </section>
    <section class="description description-partner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="training-description">
                        <h3>We would like to invite you to SALESmanago meeting for partners and resellers.</h3>

                        <p>This two-day event will take place in Cracow and provide you with all information about Marketing Automation you need. Our experts will discuss its possibilities, features, case studies and most effective selling practices.</p>
                        <p>Participation in the event is free.</p>
                        <p>Date: <strong>22-23 March 2018</strong></p>
                        <p>All Participants are also invited to a special SALESmanago After Party.</p>
                    </div>
                </div>
            </div>
            <div class="ma-triangle"></div>
        </div>
    </section>
</div>

<section id="agenda">
    <div class="container">
        <div class="loc-head agenda">Training Program</div>
    </div>
</section>

<section class="program program-klient" id="program">
    <?php get_template_part('template-parts/content', 'klient'); ?>
</section>

<section class="program program-partner" id="program">
    <?php get_template_part('template-parts/content', 'partner'); ?>
</section>

<section class="wykladowcy" id="wykladowcy">
    <div class="container">
        <div class="row">
            <div class="wy-head">
                Instructors
            </div>
        </div>
        <div class="row row-centered">
            <div class="col-md-10 col-centered">
                <div class="speaker" id="speaker-paulina">
                    <div class="row">
                        <div class="col-md-4 col-md-push-8">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pn.jpg" alt="Paulina Nowakowska" class="img-responsive img-circle center-block">
                            </div>
                        </div>
                        <div class="col-md-8 col-md-pull-4">
                            <div class="speaker-name center-mobile">Paulina Nowakowska</div>
                            <div class="speaker-desc">
                                <p>
                                    From the very beginning of her career at SALESmanago, Paulina educated clients about the possibilities and usage of the system. Initially she spread the knowledge among customers in the sales department, and now she supports Partners in the support top team. When she does not talk about the latest trends in automation, she is fascinated by the life, tradition and culture of the nations of the Far East, especially Korea (North and South).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="speaker" id="speaker-aleksander">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/as.jpg" alt="Aleksander Skałka" class="img-responsive img-circle center-block">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="speaker-name center-mobile">Aleksander Skałka</div>
                            <div class="speaker-desc">
                                <p>Marketing Automation Strategy Director. Jagiellonian University graduate. He is responsible for product development and servicing SALESmanago Projects as well as building knowledge on system’s functionalities. Has rich experience in implementing advanced automation and sales processes. After hours, a heavy metal singer.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="organisers" id="organizatorzy">
    <div class="container">
        <div class="row">
            <div class="org-head col-md-12">
                Organizers
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://www.salesmanago.com" target="blank" alt="">
                        <span class="media salesmanago"></span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://www.benhauer.com" target="blank" alt="">
                        <span class="media benhauer"></span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://education.salesmanago.com/" target="blank" alt="">
                        <span class="media edukacja"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="platform">
    <div class="container">
        <div class="row row-centered">
            <div class="col-md-10 col-centered">
                <div class="e-learning">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/graf.jpg" alt="certified specialist logo img-responsive">
                        </div>
                        <div class="col-md-8">
                            <h3>SALESmanago E-learning Platform</h3>
                            <p>Complete the course and get your <a href="http://elearning.salesmanago.com" class="free-certificate">certificate</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
