<?php

class SM_API
{
    private $clientId;
    private $apiKey;
    private $apiSecret;
    private $endpoint;
    private $owner;
    private $sha1;
    private $contactId;
    private $dateTime;
    private $email;

    private function do_post_request_sm($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        return curl_exec($ch);
    }

    function __construct($obj)
    {
        $this->email = $obj['email'];
        $this->clientId = 'gendd6kvhmfptvsn';
        $this->apiKey = 'eoijsad09883ok';
        $this->apiSecret = 'f999640d68854291a7115a0c2f0c7b08';
        $this->endpoint = "www.salesmanago.pl";
        $this->owner = 'marketing@salesmanago.pl';

        /*
        $this->email = $obj['email'];
        $this->clientId = 'r0p4lh2pw8bnyopo';
        $this->apiKey = 'eoijsad09883ok';
        $this->apiSecret = 'eg7auu0o1wfkqefil58knuv0zyuhbzuw';
        $this->endpoint = "app2.salesmanago.pl";
        $this->owner = 'slawomir.chochorek@salesmanago.pl';
        */

        $this->sha1 = sha1($this->apiKey . $this->clientId . $this->apiSecret);
        $this->dateTime = new DateTime('NOW');

        $this->upsertContact($obj);
    }

    private function setCookies(){
        setcookie('smclient', $this->contactId, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);
    }

    private function upsertContact($obj){

        $url = 'http://' . $this->endpoint .'/api/contact/upsert';
        $data = array(
            'async' => false,
            'clientId' => $this->clientId,
            'apiKey' => $this->apiKey,
            'requestTime' => time(),
            'sha' => $this->sha1,
            'contact' => array(
                'company' => $obj['company-name'],
                'email' => $this->email,
                'name' => $obj['first-name'] . ' ' . $obj['last-name'],
                'phone' => $obj['phone'],
            ),
            'owner' => $this->owner,
            'tags' => array(
                0 => $obj['tag-1'],
                1 => $obj['tag-2']
            ),
            'forceOptIn' => true,
            'forceOptOut' => false
        );

        $this->request($url, $data);
    }

    private function request($url, $data){
        $response = $this->do_post_request_sm($url, json_encode($data));
        $response = json_decode($response);

        if($response->success) {
            $this->contactId = $response->contactId;
            $this->setCookies();
        }
    }
}

