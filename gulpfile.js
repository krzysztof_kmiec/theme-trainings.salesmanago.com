const gulp = require('gulp'),
    gzip = require('gulp-gzip'),
    browserSync = require('browser-sync'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    // imageminGuetzli = require('imagemin-guetzli'),
    imageminPngquant = require('imagemin-pngquant'),
    changed = require('gulp-changed'),
    plumber = require("gulp-plumber"),
    rename = require("gulp-rename"),
    notify  = require('gulp-notify'),
    del = require('del'),
    compass = require('gulp-compass'),
    runSequence = require('run-sequence'),
    reload      = browserSync.reload;


const config = {
  dist: 'assets/',
  src: 'src/',
  fonts: 'src/fonts/*',
  scssIn: 'src/scss/main.scss',
  scssSprites: 'src/scss/compass-sprite.scss',
  scssOut: 'src/scss',
  cssOutName: 'vendors.min.css',
  cssIn: 'src/css/**/*.css',
  cssOut: 'assets/css/',
  jsCustomName: 'script.min.js',
  jsCustomIn: 'src/js/custom/*.js',
  jsVendorName: 'vendors.min.js',
  jsVendorIn: 'src/js/vendor/*.js',
  jsOut: 'assets/js/',
  imgIn: 'src/images/**/*.{jpeg,png,gif,ico}',
  jpgIn: 'src/images/**/*.jpg',
  imgOut: 'assets/images/'
};

gulp.task('sass', () => {
  return gulp.src(config.scssIn)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(sourcemaps.write({includeContent: false}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(autoprefixer({
      browsers: ['last 5 versions']
    }))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(plumber.stop())
    .pipe(gulp.dest(config.cssOut))
    .pipe(browserSync.stream())
    .pipe(reload({stream: true}))
    .pipe(notify({ message: 'Compiling SASS to CSS task is completed', onLast: true }));
});

gulp.task('sprite', () => {
  return gulp.src(config.scssSprites)
    .pipe(compass({
      css: 'src/tmp',
      sass: 'src/scss',
      image: 'src/images',
    }))
    .pipe(rename('_sprites.scss'))
    .pipe(gulp.dest(config.scssOut))
    .pipe(notify({ message: 'Sprites is generated ', onLast: true }));
});

gulp.task('css', () => {
    return gulp.src(config.cssIn)
        .pipe(plumber())
        .pipe(concat(config.cssOutName))
        .pipe(cleanCSS())
    //    .pipe(gzip())
        .pipe(plumber.stop())
        .pipe(gulp.dest(config.cssOut))
        .pipe(notify({ message: 'Concatenated CSS, task is completed', onLast: true }));
});

gulp.task('copy', () => {
   return gulp.src([config.fonts],{
       base: config.src
   })
   .pipe(gulp.dest(config.dist))
   .pipe(notify({ message: 'Copy task is completed', onLast: true }));;
});

gulp.task('jsCustom', () => {
  return gulp.src(config.jsCustomIn)
    .pipe(plumber())
    .pipe(concat(config.jsCustomName))
    .pipe(uglify({preserveComments: 'license'}))
    .pipe(plumber.stop())
    .pipe(gulp.dest(config.jsOut))
    .pipe(notify({ message: 'Concatenated jsCustom, task is completed', onLast: true }));
});

gulp.task('jsVendor', () => {
    return gulp.src(config.jsVendorIn)
        .pipe(plumber())
        .pipe(concat(config.jsVendorName))
     //   .pipe(uglify({preserveComments: 'license'}))
        .pipe(plumber.stop())
        .pipe(gulp.dest(config.jsOut))
        .pipe(notify({ message: 'Concatenated jsVendor, task is completed', onLast: true }));
});


gulp.task('img', () => {
  return gulp.src(config.imgIn)
    .pipe(changed(config.imgOut))
    .pipe(imagemin({
      optimizationLevel: 10,
      progressive: true,
      interlaced: true,
      use: [imageminPngquant()]
    }))
    .pipe(gulp.dest(config.imgOut))
    .pipe(notify({ message: 'Optimized img, task is completed', onLast: true }));;;
});

gulp.task('jpg', () => {
  return gulp.src(config.jpgIn)
    .pipe(imagemin([]))
    .pipe(gulp.dest(config.imgOut))
    .pipe(notify({ message: 'Optimized jpg, task is completed', onLast: true }));;;
});


gulp.task( 'tmp', () => {
   return del('src/tmp');
});


gulp.task('clean', () => {
  return del([config.dist]);
});

gulp.task('build', () => {
    runSequence('clean', ['sprite','sass','tmp','css','img','jpg','copy','jsVendor','jsCustom']);
});

gulp.task('reload', () => {
    browserSync.reload();
});

gulp.task('serve', ['sass'], () => {

  var files = [
    './style.css',
    './*.php'
    ];
        browserSync.init(files, {
    //browsersync with a php server
    proxy: "http://localhost/workshops/",
    notify: false
    });
    gulp.watch('src/scss/*.scss', ['sass']);

});


gulp.task('default', ['serve'], () =>{
    gulp.watch(config.scssIn, ['sass']);
});