<section id="location">
    <div class="localization localization--client">
        <div class="container localization__container localization__container--warsaw">
            <div class="row">
                <div class="loc-head">
                    Location
                </div>
                <h3>COMING SOON</h3>
            </div>
        </div>
    </div>
    <div class="localization localization--partner">
        <div class="container localization__container localization__container--warsaw">
            <div class="row">
                <div class="loc-head">
                    Location
                </div>
            </div>
            <div class="row row-centered">
                <div class="col-md-6 col-centered">
                    <div class="text-center">
                        <h3>BENHAUER</h3>
                        <p>Grzegórzecka 21</p>
                        <p>Cracow</p>
                    </div>
                </div>
            </div>
            <!-- MAPKA GOOGLE -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2561.4973964230558!2d19.950620751041033!3d50.05824587932221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47165b0818a7544d%3A0x7648218ea0f10363!2sBenhauer+Sp.+z+o.o.!5e0!3m2!1spl!2spl!4v1503671763657" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
            <!-- KONIEC MAPKI -->
        </div>
    </div>
</section>

<div class="foot-social">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="salesmanago-big"></span>
            </div>
            <div class="col-md-8">
                <div class="social">
                    <div class="social_icons_down clearfix">
                        <span class="follow-us">Follow us: </span>
                        <a target="_blank" href="https://www.facebook.com/SALESmanago">
                        <span class="facebook"></span>
                        </a>
                        <a target="_blank" href="https://www.linkedin.com/company/5134015/">
                        <span class="linkedin"></span>
                        </a>
                        <a target="_blank" href="https://www.youtube.com/channel/UC0-su31DYGaQ3cLmbUnCOPA">
                        <span class="youtube"></span>
                        </a>
                        <a target="_blank" href="https://twitter.com/SALESmanago">
                        <span class="twitter"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-buttons">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="https://www.salesmanago.com" target="_blank">
                    <div class="foot-button">
                        <div class="foot-button-text">Marketing automation system</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://blog.salesmanago.com" target="_blank">
                    <div class="foot-button">
                        <div class="foot-button-text">Marketing automation blog</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="https://www.salesmanago.com/marketing-automation/email_marketing.htm" target="_blank">
                    <div class="foot-button">
                        <div class="foot-button-text">Email marketing automation</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <ul>
                    <li>©
                        <?php echo date('Y'); ?> SALESmanago</li>
                    <li>All rights reserved</li>
                    <li>this website uses cookies</li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script>
    (function() {

        $('.open-register').on('click', function() {
            $('#register').toggleClass('form__content--active');
            if ($('#billing-info').hasClass("hide")) {
                setTimeout(function() {
                    $('#billing-info').removeClass('hide');
                    $('#recurly').addClass('hide');
                    $('#paypal').addClass('hide');
                }, 1500)
            }
        });

        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 80
                    }, 1000);
                    return false;
                }
            }
        });

    })();

</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>

<!--

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/iframeResizer.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jssor.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jssor.slider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>

-->
<?php require_once('analytics-scripts/salesmanago.php'); ?>
<?php require_once('analytics-scripts/googleanalytics.php'); ?>

<script>
    (function() {

        var btnList = $(".item-list");
        btnList.delegate("input", "focusout", function() {
            var id = $(this).attr('id').toString().slice(0, -3);
            var text = $(this).val();
            $('#' + id).val(text);
        });

        btnList.delegate("select", "focusout", function() {
            var id = $(this).attr('id').toString().slice(0, -3);
            var text = $(this).val();
            $('#' + id).val(text);
        });

        var btn = document.getElementById('payment-chosen');

        btn.addEventListener('click', function() {

            var paymentMethod = $('.payment__method input[name=payment]:checked').val();
            $('#form').attr('action', paymentMethod);

            /*
            if(paymentMethod === 'card'){
                $('#billing-info').addClass('hide');
                $('#recurly').removeClass('hide');
                $(document).scrollTo('#recurly');
                console.log('card');
            }else if(paymentMethod === 'paypal'){
                $('#billing-info').addClass('hide');
                $('#paypal').removeClass('hide');
                $(document).scrollTo('#paypal');
                console.log('paypal');
            }
            */

            //            console.log('test');
            //           $('.payment__method input').on('change', function() {
            //               console.log($('input[name=payment]:checked').val());
            //           });

        });

        $('.description-klient, .program').hide();
        $('.program-partner, .description-partner').show();
        $('.agenda').text('Training Program 22-23.03.2018');
        $('.partner-link').addClass('active');
        $('.localization').hide();
        $('.localization--partner').show();

        $('#speaker-aleksander').hide();

        $('.post-tab-button-pierwszy').on('click', function() {
            $('#speaker-aleksander').hide();
        });
        // $('.post-tab-button-drugi').on('click', function() {
        //     $('#speaker-aleksander').show();
        // });

        console.log($('.post-tab-button').text());

        $('.agenda-link').on('click', function() {
            $('.agenda-link').removeClass('active');
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
                if ($('.partner-link').hasClass('active')) {
                    $('.open-register').removeClass('hidden');
                } else {
                    $('.open-register').addClass('hidden');
                }
            }
        });

        $('.klient-link').on('click', function() {
            $('.program, .description').hide();
            $('.agenda').text('Training Program');
            $('.program-klient, .description-klient').fadeIn();
            $('.localization').hide();
            $('.localization--client').show();

        });

        $('.partner-link').on('click', function() {
            $('.program, .description').hide();
            $('.agenda').text('Training Program 22-23.03.2018');
            $('.program-partner, .description-partner').fadeIn();
            $('.localization').hide();
            $('.localization--partner').show();
        });

        //$("#country option[value='AF']").attr("selected", true);

        //questions.children().eq($(this).index() + 1).slideToggle('slow');
        /*
         if($(this).hasClass('showAnswer')){
         $(this).removeClass('showAnswer');
         console.log('hide');
         questions.children().eq($(this).index() + 1).hide(300);
         }else{
         console.log('show');
         $( this ).addClass('showAnswer');
         questions.children().eq($(this).index() + 1).slideDown(700);

         }
         */

        /*
        console.log('Test');
        itemList = document.querySelector(".item-list");

        // Event delegation
        itemList.addEventListener('click', function (ev) {
            if (ev.target.nodeName === 'LI') {
                alert('item new');
            }
        }, false);
        */
    })();

</script>
</body>

</html>
