<div id="billing-info" class="recurly__form">
    <div class="container text-center">
        <h3>Registration form</h3>
        <p class="form__text">Below, you will find the registration form for the training. You will recieve an email form us with all the details a few days before the event.</p>
        <p class="form__text">Note: there is an upper limit on the number of guests - up to 15 attendees.</p>

        <div class="row" style="padding-top: 20px">

            <form id="form" method="post" action="registration" novalidate>

                <!--
                <div class="col-xs-12">
                    <h3>Dane personalne</h3>
                </div>
                -->

                <div class="col-md-6 col-sm-12">

                    <div>
                        <label for="company_name">Company Name</label>
                        <input type="text" name="company-name">
                    </div>

                    <div>
                        <label for="first_name">First Name<span>*</span></label>
                        <input type="text" name="first-name" data-error-message="first name" required>
                    </div>

                    <div>
                        <label for="last_name">Last Name<span>*</span></label>
                        <input type="text" name="last-name" data-error-message="last name" required>
                    </div>

                </div>
                <div class="col-md-6 col-sm-12">

                    <div>
                        <label for="email">Email address<span>*</span></label>
                        <input type="email" id="email" name="email" data-error-message="email" required>
                    </div>

                    <div>
                        <label for="phone">Phone Number<span>*</span></label>
                        <input type="text" name="phone" data-error-message="phone" required>
                    </div>

                    <div>
                        <label for="trainings">Training Selection<span>*</span></label>
                        <select class="select__country" id="selectTraining" name="trainings" data-error-message="custom details" required>
                            <option value=""></option>
                            <?php /* <option value="szkolenia_klientow_eng_09.2017">For clients</option> */ ?>
                            <option>For partners</option>
                        </select>
                        <input type="hidden" id="tag-sm-1" name="tag-sm-1" value="">
                        <input type="hidden" id="tag-sm-2" name="tag-sm-2" value="">
                    </div>

                </div>
                <div class="col-xs-12">
                    <div class="col-md-6 col-md-offset-3 col-sm-12">
                        <button type="submit" id="payment-chosen" style="margin-top: 20px;width: 160px;">Send</button>

                        <!--
                        <div class="form-group" style="text-align: left;margin-left: 30px;">
                            <div class="checkbox">
                                <input type="checkbox" data-error-message="accetpt regulations" required checked>Akceptuję <a href="<?php echo get_page_link(15); ?>" target="_blank">regulamin</a><span>*</span>
                            </div>
                        </div>
                        -->

                        <p style="text-align: center; padding-top: 15px;">*Indicates a required field</p>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="alert"></div>
                </div>

            </form>

        </div>

    </div>
</div>
<script>
    (function() {

        function addSmTag() {
            var select = document.getElementById('selectTraining');
            var selectedOption = select.options[select.selectedIndex].value;

            var tag1 = document.getElementById('tag-sm-1');
            var tag2 = document.getElementById('tag-sm-2');

            if (selectedOption === 'For partners') {
                tag1.value = 'szkolenia_partnerskie_eng';
                tag2.value = 'szkolenia_partnerskie_eng_03.2018';
            } else {
                tag1.value = '';
                tag2.value = '';
            }
        }

        document.getElementById('selectTraining').addEventListener('change', function() {
            addSmTag();
        });

        document.getElementById('form').addEventListener('submit', function() {
            var data = {
                email: document.getElementById('email').value
            }

            $.ajax({
                type: 'POST',
                url: '<?php echo get_template_directory_uri(); ?>/sendEmail.php',
                data: data
            });
        });

        function Validator(form) {
            this.form = form;
            this.fields = this.form.querySelectorAll("[required]");
            this.errors = [];
            this.errorsList = this.form.querySelector(".alert");

            if (!this.fields.length) return;

            this.form.onsubmit = function(e) {
                e.preventDefault();

                var formValid = this.validate();

                if (formValid) {
                    addSmTag();
                    this.form.submit();
                } else {
                    return false;
                }
            }.bind(this);
        }

        Validator.prototype.validate = function() {
            this.clearErrors();

            for (var i = 0; i < this.fields.length; i++) {
                this.validateField(this.fields[i]);
            }

            if (!this.errors.length) {
                return true;
            } else {
                this.showErrors();
                return false;
            }
        };

        Validator.prototype.validateField = function(field) {
            var fieldValid = field.validity.valid;

            if (fieldValid) {
                this.markAsValid(field);
            } else {
                this.errors.push(field.dataset.errorMessage);
                this.markAsInvalid(field);
            }
        };

        Validator.prototype.markAsValid = function(field) {
            field.classList.remove("invalid");
            field.classList.add("valid");
        };

        Validator.prototype.markAsInvalid = function(field) {
            field.classList.remove("valid");
            field.classList.add("invalid");
        };

        Validator.prototype.showErrors = function() {
            var msg = 'The following fields appear to be invalid: ';

            for (var i = 0; i < this.errors.length; i++) {
                msg += this.errors[i];
                msg += ', ';
            }
            msg = msg.slice(0, -2);

            this.errorsList.innerHTML = msg;
            this.errorsList.style.display = "block";
        };

        Validator.prototype.clearErrors = function() {
            this.errors.length = 0;
        };

        var validator1 = new Validator(document.querySelector("#form"));

    })();

</script>
