<div class="container">
	<div class="row row-centered">
		<div class="col-md-10 col-centered">
			<div class="schedule-container">
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    11:00am - 11:30am
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                        Marketing Automation - introduction
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    11:30am - 12:00pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                       Website integration and the account configuration  
			                    </div>
			                    <div class="schedule-desc">
			                        <ul>
			                            <li><i class="fa fa-play-circle"></i> contact monitoring: how it works  </li>
			                            <li><i class="fa fa-play-circle"></i> integration with the shop platform / page www</li>
			                            <li><i class="fa fa-play-circle"></i> email account and user account configuratio</li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    12:00pm - 12:05pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick schedule-tick-disabled visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title center-mobile">
			                        Coffee break
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    12:05pm - 1:00pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                       Contact database and lead acquisition    
			                    </div>
			                    <div class="schedule-desc">
			                        <ul>
			                            <li><i class="fa fa-play-circle"></i> CRM: how to import contacts and read the contact card information correctly </li>
			                            <li><i class="fa fa-play-circle"></i> Ways of acquiring new contacts</li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    1:00pm - 2:00pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick schedule-tick-disabled visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title center-mobile">
			                        Lunch
			                    </div>

			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                 2:00pm - 3:30pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                 		Communication - mass and automatic campaigns   
			                    </div>
			                    <div class="schedule-desc">
			                        <ul>
			                            <li><i class="fa fa-play-circle"></i> Email Marketing: creating messages and sending methods (+ dynamic content ) </li>
			                            <li><i class="fa fa-play-circle"></i> Automation processes and contact segmentation </li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    3:30pm - 3:35pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
									Coffee break
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="schedule-time">
			                    3:35pm - 4:30pm
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick schedule-tick-disabled visible-lg">

			                </div>
			            </div>
			            <div class="col-md-8">
			                <div class="schedule-content-box">
			                    <div class="schedule-title center-mobile">
			                        Case Study / workshops 
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			</div>
		</div>
	</div>
</div>
