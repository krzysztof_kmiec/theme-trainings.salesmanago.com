    <div class="container">
        <div class="row row-centered">
            <div class="col-centered col-md-10">

                <div class="post-tabs-container">
                    <div class="post-tab-buttons">
                        <div class="post-tab-button post-tab-button-pierwszy active" data-tab="pierwszy">Day One</div>
                        <div class="post-tab-button post-tab-button-drugi" data-tab="drugi">Day Two</div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="post-tab-content post-tab-content-pierwszy">
                        <div class="schedule-container">
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">
                                            9:00am - 9:30am
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">
                                                Coffee and networking
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">
                                            9:30am - 10:45am
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Introduction to Marketing Automation: overview, possibilities and main functionalities of the tool.</div>
                                            <!-- <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Tomasz Świątek, Marketing Automation Consulting Manager, SALESmanago Marketing Automation</li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">
                                            10:45am - 12:00pm
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick schedule-tick-disabled visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title center-mobile">SALESmanago Marketing Automation step by step. How to configure and use the system on a regular basis.</div>
                                            <!-- <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Aleksander Skałka, Marketing Automation Strategy Director, SALESmanago Marketing Automation</li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">12:00pm - 1:00pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Lunch</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">1:00pm - 2:00pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick schedule-tick-disabled visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title center-mobile">Lead Nurturing and other Marketing Automation best practices and case studies in B2B.</div>
                                           <!--  <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Tomasz Świątek, Marketing Automation Consulting Manager, SALESmanago Marketing Automation</li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">2:00pm - 2:30pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Coffee Break</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">2:30pm - 3:30pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Marketing Automation best practices and case studies in B2C and eCommerce.</div>
                                            <!-- <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Aleksander Skałka, Marketing Automation Strategy Director, SALESmanago Marketing Automation</li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">3.30pm - 4.30pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick schedule-tick-disabled visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title center-mobile">Beacons, Anonymous Marketing Automation, NextGen for eCommerce, ROPO effect handling and other advanced possibilities of SALESmanago.</div>
                                           <!--  <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Tomasz Świątek, Marketing Automation Consulting Manager, SALESmanago Marketing Automation</li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">8:00pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">SALESmanago After Party</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-tab-content post-tab-content-drugi">
                        <div class="schedule-container">
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">
                                            10:00am - 11:00am
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Benhauer / SALESmanago as a Marketing Automation leader in Europe.</div>
                                            <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Grzegorz Błażewicz, CEO, SALESmanago Marketing Automation</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">
                                            11:00am - 12:00pm
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick schedule-tick-disabled visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title center-mobile">Partner Program Policies, Provisions and Revenue Simulation.</div>
                                            <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Radosław Gawlas, Head of International Sales, SALESmanago Marketing Automation </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">12:00pm - 1:00pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Lunch</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">1:00pm - 2:00pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick schedule-tick-disabled visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title center-mobile">How to successfully sell Marketing Automation tool in B2B, B2C and eCommerce. Best practices and case studies.</div>
                                            <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Michał Janas, Head of International Partnership Channel, SALESmanago Marketing Automation </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wpis -->
                            <div class="schedule-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="schedule-time">2:00pm - 2:45pm</div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="schedule-tick visible-lg">

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="schedule-content-box">
                                            <div class="schedule-title">Introduction to Mobile Marketing Automation: business overview, case studies and reselling potential.</div>
                                            <div class="schedule-desc">
                                                <ul>
                                                    <li><i class="fa fa-play-circle"></i> Michał Loręcik, Partner Channel Manager</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="ma-triangle"></div>