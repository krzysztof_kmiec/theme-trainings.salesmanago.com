<?php
/**
 * Template Name: registration
 **/

if(!isset($_POST['first-name'])){
    header("Location: http://trainings.salesmanago.com");
}

get_template_part('analytics-scripts/salesmanago', 'integration');

$obj = array(
    'company-name' => (isset($_POST['company-name'])) ? htmlspecialchars($_POST['company-name']) : '',
    'first-name' => htmlspecialchars($_POST['first-name']),
    'last-name' => htmlspecialchars($_POST['last-name']),
    'email' => htmlspecialchars($_POST['email']),
    'phone' => htmlspecialchars($_POST['phone']),
    'tag-1' => htmlspecialchars($_POST['tag-sm-1']),
    'tag-2' => htmlspecialchars($_POST['tag-sm-2'])
);

$sm_user = new SM_API($obj);

?>

<?php get_header(); ?>

<div class="container">
    <div class="ma-head" style="padding: 50px 0px; font-size: 24px; line-height: 28px; margin-top: 90px;">
        Thank you for your registration.<br><br>Feel free to contact us should you have any questions success@salesmanago.com<br><br>Don't forget to bring your laptop! See you soon on the training!
    </div>
</div>


<div id="register" class="form__content" style="padding-top: 0;">
    <?php get_template_part('template-parts/content', 'billing-info'); ?>
</div>

<section class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="banner-heading">Trainings for the <br> Clients and Partners </h1>
                <img class="sales-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/smlogo.png" alt="SALESmanago logo">
            </div>
        </div>
    </div>
</section>


<section class="description description-partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="training-description">
                    <h3>We would like to invite you to SALESmanago meeting for partners and resellers.</h3>

                    <p>This one-day event will take place in Cracow and provide you with all information about Marketing Automation you need. Our experts will discuss its possibilities, features, case studies and most effective selling practices.</p>
                    <p>Participation in the event is free.</p>
                    <p>Date: <strong>19-20.10.2017.</strong></p>
                    <p>All Participants are also invited to a special SALESmanago After Party.</p>
                </div>
            </div>
        </div>
        <div class="ma-triangle"></div>
    </div>
</section>


<section class="wykladowcy" id="wykladowcy">
    <div class="container">
        <div class="row">
            <div class="wy-head">
                Instructors
            </div>
        </div>
        <div class="row row-centered">
            <div class="col-md-10 col-centered">
                <div class="speaker" id="speaker-paulina">
                    <div class="row">
                        <div class="col-md-4 col-md-push-8">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pn.jpg" alt="Paulina Nowakowska" class="img-responsive img-circle center-block">
                            </div>
                        </div>
                        <div class="col-md-8 col-md-pull-4">
                            <div class="speaker-name center-mobile">Paulina Nowakowska</div>
                            <div class="speaker-desc">
                                <p>
                                   From the very beginning of her career at SALESmanago, Paulina educated clients about the possibilities and usage of the system. Initially she spread the knowledge among customers in the sales department, and now she supports Partners in the support top team. When she does not talk about the latest trends in automation, she is fascinated by the life, tradition and culture of the nations of the Far East, especially Korea (North and South).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="speaker" id="speaker-aleksander">
                    <div class="row ">
                        <div class="col-md-4">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/as.jpg" alt="Aleksander Skałka" class="img-responsive img-circle center-block">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="speaker-name center-mobile">Aleksander Skałka</div>
                            <div class="speaker-desc">
                                <p>Marketing Automation Strategy Director. Jagiellonian University graduate. He is responsible for product development and servicing SALESmanago Projects as well as building knowledge on system’s functionalities. Has rich experience in implementing advanced automation and sales processes. After hours, a heavy metal singer.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="organisers" id="organizatorzy">
    <div class="container">
        <div class="row">
            <div class="org-head col-md-12">
                Organizers
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://www.salesmanago.com" target="blank" alt="">
                        <span class="media salesmanago"></span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://www.benhauer.com" target="blank" alt="">
                        <span class="media benhauer"></span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://education.salesmanago.com/" target="blank" alt="">
                        <span class="media edukacja"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="platform">
    <div class="container">
        <div class="row row-centered">
            <div class="col-md-10 col-centered">
                <div class="e-learning">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/graf.jpg" alt="certified specialist logo img-responsive">
                        </div>
                        <div class="col-md-8">
                            <h3>SALESmanago E-learning Platform</h3>
                            <p>Complete the course and get your <a href="http://elearning.salesmanago.com" class="free-certificate">certificate</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
