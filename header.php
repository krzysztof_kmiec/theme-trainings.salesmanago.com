<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Marketing Automation Trainings- SALESmanago</title>

    <meta property="og:title" content="Marketing Automation Day" />
    <meta property="og:type" content="website" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,400&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
        <!--

                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,400&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="css/main.css" />

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    -->
<meta name="msvalidate.01" content="37D0531AF227E3A03DAC1E0FFF991006" />
<meta name="google-site-verification" content="V-8s2m0QjLmy_yOVSGG5okV1dyzHa23_XKBqlMu0mF0" />

<?php wp_head(); ?>

</head>
<body>

    <?php require_once('analytics-scripts/googletagmanager.php'); ?>

    <header class="header">
        <div class="container">
            <div class="row header-top">
                <div class="col-md-4 header-workshops">
                    <div class="logo-green">Marketing Automation</div>
                    <div class="logo-bottom">
                        <div class="logo-black">TRAININGS</div>
                        <div class="logo-desc"></div>
                    </div>
                </div>
                <div class="col-md-8 text-right header-top__nav">
                    <nav class="site-nav">
                        <ul>
                            <li><a href="<?php echo get_site_url(); ?>/#description">Event Description</a></li>
                            <li><a href="<?php echo get_site_url(); ?>/#agenda">Training Program</a></li>
                            <li><a href="<?php echo get_site_url(); ?>/#location">Location</a></li>
                            <li><a href="<?php echo get_site_url(); ?>/#register" class="open-register">Register</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>